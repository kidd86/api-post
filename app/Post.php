<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = [
        'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];


    public function post()
    {
        return $this->belongsTo('App\User');
    }
}
