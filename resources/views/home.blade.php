@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <p style="padding-top: 50px;">
                        Your API Token: {{ Auth::user()->api_token }}
                    </p>

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Data</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($posts as $post)

                            <tr>
                                <td>{{ json_encode($post->data) }}</td>
                                <td>{{ $post->created_at }}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
